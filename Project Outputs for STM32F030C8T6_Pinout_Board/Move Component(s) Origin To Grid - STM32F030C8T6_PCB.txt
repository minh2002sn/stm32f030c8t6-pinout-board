Move Component(s) Origin to Grid
Filename     : D:\A_Minh\B_Altium_Projects\Study_PCB\STM32F030C8T6_Pinout_Board\STM32F030C8T6_Pinout_Board\STM32F030C8T6_PCB.PcbDoc
Date         : 8/16/2022
Time         : 10:14:13 PM
Time Elapsed : 00:00:00


    SMT Small Component R7-RESISTOR SMD (7.747mm,-15.748mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component R5-RESISTOR SMD (7.747mm,-8.763mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    Small Component Y1-8MHz (17.018mm,-14.986mm) on Top Layer - Component pads are OFF grid. No action taken.
    Component U1-STM32F030C8T6 (28.956mm,-14.224mm) on Top Layer - Component pads are OFF grid. No action taken.
    SMT Small Component S1-Tact SW - 2Pin (41.529mm,-15.113mm) on Top Layer - Component pads are OFF grid. No action taken.
    SMT Small Component R9-RESISTOR SMD (20.066mm,-15.494mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component R3-RESISTOR SMD (50.673mm,-22.225mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component R2-RESISTOR SMD (50.673mm,-17.399mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component R1-RESISTOR SMD (50.673mm,-19.812mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component R8-RESISTOR SMD (27.601mm,-23.241mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component R6-RESISTOR SMD (24.892mm,-23.241mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component R4-RESISTOR SMD (33.02mm,-23.241mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SIP Component P4-61300411021 (2.54mm,-11.684mm) on Top Layer - Component pads are OFF grid. No action taken.
    SIP Component P3-61302011121 (91.948mm,2.54mm) on Top Layer - Component pads are OFF grid. No action taken.
    SIP Component P2-61302011121 (91.948mm,-17.907mm) on Top Layer - Component pads are OFF grid. No action taken.
    DIP Component P1-61300621121 (41.529mm,-20.447mm) on Top Layer - Component pads are OFF grid. No action taken.
    Component J1-629105136821 (57.277mm,-15.437mm) on Top Layer - Component pads are OFF grid. No action taken.
    Component IC1-LM1117 (41.783mm,-12.192mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component D2-LED SMD (7.747mm,-19.177mm) on Top Layer - Component pads are OFF grid. No action taken.
    SMT Small Component D1-LED SMD (7.62mm,-5.08mm) on Top Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C16-Cap Non-Pol SMD (14.732mm,-19.685mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C15-Cap Non-Pol SMD (2.794mm,-20.447mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C14-Cap Non-Pol SMD (14.986mm,-10.287mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C5-Cap Non-Pol SMD (39.243mm,-4.851mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C4-Cap Non-Pol SMD (41.783mm,-7.112mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C3-Cap Non-Pol SMD (47.371mm,-9.906mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C2-Cap Non-Pol SMD (49.784mm,-12.192mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C13-Cap Non-Pol SMD (36.83mm,-7.112mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C12-Cap Non-Pol SMD (34.14mm,-7.112mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C11-Cap Non-Pol SMD (31.45mm,-7.112mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C10-Cap Non-Pol SMD (28.76mm,-7.112mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C9-Cap Non-Pol SMD (26.044mm,-4.851mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C8-Cap Non-Pol SMD (23.354mm,-4.851mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C7-Cap Non-Pol SMD (17.974mm,-4.851mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C6-Cap Non-Pol SMD (20.664mm,-4.851mm) on Bottom Layer - Component pads are OFF grid. No action taken.
    SMT Small Component C1-Cap Non-Pol SMD (30.311mm,-23.241mm) on Bottom Layer - Component pads are OFF grid. No action taken.
count : 36